package com.falathon.tribeoftech.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.falathon.tribeoftech.R

class SplashActivity : Activity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.SplashTheme)

        run {
            Thread.sleep(1500)
        }

        val intent = Intent(this, WebViewActivity::class.java)
        startActivity(intent)
        finish()
    }

}
package com.falathon.tribeoftech.presentation

import com.falathon.tribeoftech.R
import com.falathon.tribeoftech.databinding.ActivityMainWebViewBinding
import com.falathon.tribeoftech.presentation.base.BaseActivity
import com.falathon.tribeoftech.utils.MyWebViewClient
import com.falathon.tribeoftech.utils.WebViewChromeClient

class WebViewActivity :BaseActivity<ActivityMainWebViewBinding>(){

    private val url = "https://demosdesarrollos.com.ar/falaplay/"

    override fun getLayoutRes(): Int {
        return R.layout.activity_main_web_view
    }

    override fun bind(bind: ActivityMainWebViewBinding) {

        val webView = bind.webView

        val client = MyWebViewClient()
        val chromeClient = WebViewChromeClient()

        webView.webChromeClient = chromeClient
        webView.webViewClient = client

        webView.clearCache(true)
        webView.clearHistory()
        webView.settings.javaScriptEnabled = true

        webView.loadUrl(url)

    }

}
package com.falathon.tribeoftech.presentation.base

import android.app.Activity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding


abstract class BaseActivity<T : ViewDataBinding> : Activity() {

    abstract fun getLayoutRes(): Int
    abstract fun bind(bind: T)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view = DataBindingUtil.setContentView<T>(this, getLayoutRes())
        bind(view)
    }


}